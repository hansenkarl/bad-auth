Here are a couple of notes and comments regarding project AUTH.

I assume, that this task was deliberately written to be, like it is.

In order, to have a strong encryption for passwords, it is always best to use reliable libraries, that let you manage the security and keep you as a user from making serious mistakes.

I tried several appropaches for this task. The one, I would have liked to use, required the usage of third party library called bcrypt (included in project). If you would like to try and see, how it encrypts, then remove comment slashes in file Accounts.java. The lines to remove comments from are 7, 77, 82 and 83. Also comment out the lines which use Caesar's cipher, the encryption method used to encrypt data in this program at the moment (109 and 115). In real world applications, Caesar's cipher should never be used!

The reason, I chose not to send this solution with bcrypt enabled, is because of PersistentTest.java. In this file, there is a method called guess(). While using bcrypt library, this code would keep running for a very long time, which would render it pointless.

However, you can let it run for a couple of seconds to see accounts.acc have proper encryption.

The entire projects was not formatted according to Java's traditional formatting rules, thus I let Visual Studio Code, to automatically format and fix everything. I would also organize my files in a way that public methods would appear before the private methods (private methods in the end of the file). Same would apply to package-private methods, although I would place them before private methods.

The storage could also have been done better, but I assume, it was deliberate, to create the .acc files inside CreateInitial.java and PersistenTest.java. Where they not in there, I could have created an another .acc file, which I could use to check, if the password had been updated or not.
