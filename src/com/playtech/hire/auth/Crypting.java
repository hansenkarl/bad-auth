package com.playtech.hire.auth;

public class Crypting {

	public static String caesarsChiper(String plaintext) {
		StringBuilder encrypted = new StringBuilder();
		for (char character : plaintext.toCharArray()) {
			if (character != ' ') {
				int originalAlphabetPosition = character - 'a';
				int newAlphabetPosition = (originalAlphabetPosition + 7) % 26;
				char newCharacter = (char) ('a' + newAlphabetPosition);
				encrypted.append(newCharacter);
			} else {
				encrypted.append(character);
			}
		}
		return encrypted.toString();
	}
}
